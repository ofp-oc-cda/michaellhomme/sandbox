# Sandbox

# Use case

```plantuml
left to right direction
actor Guest as g
package Professional {
  actor Chef as c
  actor "Food Critic" as fc
}
package Restaurant {
  usecase "Eat Food" as UC1
  usecase "Pay for Food" as UC2
  usecase "Drink" as UC3
  usecase "Review" as UC4
}
fc --> UC4
g --> UC1
g --> UC2
g --> UC3
```

# Class diagram

```plantuml
class User {
  int id
  String name
}

class Team {
  int id
  String name
}

class Game {
  int id
  String name
}

User "0.*" - "0.*" Team
User "0.*" - "0.*" Game
Team "0.*" - "0.*" Game
```

# Sequence diagram

```plantuml
User -> GUI: Select matching view
GUI --> User: Display user profile

alt not interested in profile
  User -> GUI: click skip profile
  GUI --> User: display another profile
else profile match
  User -> GUI: click more details
  GUI --> User: flip card and display details

  alt not interested in profile
    User -> GUI: click skip profile
    GUI --> User: display another profile
  else
    User -> GUI: match profile
    GUI --> User: display another profile
  end
end
```

# Flow chart

```plantuml
[*] --> ProfileMatcher

state DisplayProfile
DisplayProfile: display generic info

state RejectProfile
RejectProfile: store info

state DisplayProfileDetails
DisplayProfileDetails: card flipped to display more information

state AcceptProfile
AcceptProfile: store info / send message ?

state choice <<choice>>
state validate <<choice>>

ProfileMatcher --> DisplayProfile : fetch next profile
DisplayProfile --> choice : interesting ?
choice --> RejectProfile : no
choice --> DisplayProfileDetails : maybe
RejectProfile --> ProfileMatcher
DisplayProfileDetails --> validate : still interested ?
validate --> ProfileMatcher : forget it...
validate --> AcceptProfile : yes
AcceptProfile --> ProfileMatcher
```
